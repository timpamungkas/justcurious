package com.timpamungkas.oauth.rest.domain;

public enum Gender {
	MALE, FEMALE
}
