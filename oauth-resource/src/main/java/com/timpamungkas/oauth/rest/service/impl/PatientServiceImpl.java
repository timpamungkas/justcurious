package com.timpamungkas.oauth.rest.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Service;

import com.timpamungkas.oauth.rest.domain.Gender;
import com.timpamungkas.oauth.rest.domain.Patient;
import com.timpamungkas.oauth.rest.domain.Status;
import com.timpamungkas.oauth.rest.service.PatientService;

@Service
public class PatientServiceImpl implements PatientService {

	@Override
	public List<Patient> findAll() {
		List<Patient> list = new ArrayList<>();
		Patient patient1 = new Patient();
		Patient patient2 = new Patient();

		patient1.setPatientId(1L);
		patient1.setFirstName("Ben");
		patient1.setLastName("Franklin");
		patient1.setGender(Gender.MALE);
		patient1.setMedicalOrder("Medicine1");
		patient1.setDosage("7 x 10ml");
		patient1.setStatus(Status.ACTIVE);

		patient2.setPatientId(2L);
		patient2.setFirstName("John");
		patient2.setLastName("Adams");
		patient2.setGender(Gender.MALE);
		patient2.setMedicalOrder("Medicine2");
		patient2.setDosage("12 x 5ml");
		patient2.setStatus(Status.ACTIVE);

		list.add(patient1);
		list.add(patient2);

		return list;
	}

}
