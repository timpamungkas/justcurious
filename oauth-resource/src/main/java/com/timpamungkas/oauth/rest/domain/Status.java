package com.timpamungkas.oauth.rest.domain;

public enum Status {
	ACTIVE, INACTIVE
}
