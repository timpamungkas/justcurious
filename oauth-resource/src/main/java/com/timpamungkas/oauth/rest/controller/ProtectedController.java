package com.timpamungkas.oauth.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.timpamungkas.oauth.rest.domain.Patient;
import com.timpamungkas.oauth.rest.service.PatientService;

@RestController
@RequestMapping("/api/")
public class ProtectedController {

	@Autowired
	private PatientService patientService;

	@GetMapping("/patients")
	public List<Patient> findAll() {
		return patientService.findAll();
	}

}
