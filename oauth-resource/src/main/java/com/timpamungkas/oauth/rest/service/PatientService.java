package com.timpamungkas.oauth.rest.service;

import java.util.List;

import com.timpamungkas.oauth.rest.domain.Patient;

public interface PatientService {

	List<Patient> findAll();

}
