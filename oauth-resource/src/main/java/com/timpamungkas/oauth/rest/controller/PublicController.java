package com.timpamungkas.oauth.rest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/public")
public class PublicController {

	@GetMapping("/resource1")
	public String resource1() {
		return "This is public resources, no need authorization";
	}
	
}
