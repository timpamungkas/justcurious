package com.timpamungkas.oauth.client.service;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.json.JSONException;
import org.json.JSONObject;

import com.timpamungkas.oauth.client.domain.Patient;

public interface HttpService {

	JSONObject getToken(String authCode) throws IOException, JSONException;

	List<Patient> getResource(String token) throws IOException, JSONException;

	String getPublicInfo() throws IOException;

	List<Patient> responseParser(CloseableHttpResponse httpResponse) throws IOException, JSONException;
	
}
