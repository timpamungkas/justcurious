package com.timpamungkas.oauth.client.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.timpamungkas.oauth.client.domain.PlainString;
import com.timpamungkas.oauth.client.json.base.JSONBaseError;
import com.timpamungkas.oauth.client.json.base.JSONBaseHeader;
import com.timpamungkas.oauth.client.json.base.JSONBaseResponse;
import com.timpamungkas.oauth.client.util.HttpUtil;
import com.timpamungkas.oauth.client.util.LogUtil;

@RestController
@RequestMapping("/api/public/elk")
public class ElkController {

	private static final Logger logger = LoggerFactory.getLogger(ElkController.class);
	private static final LogUtil LOG = new LogUtil(logger);

	@GetMapping("/tests/{id}")
	public ResponseEntity<JSONBaseResponse<PlainString>> tests(@PathVariable int id, HttpServletRequest request) {
		long start = System.currentTimeMillis();

		try {
			Thread.sleep((long) (Math.random() * 200));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		JSONBaseResponse<PlainString> result = new JSONBaseResponse<>();
		JSONBaseHeader header = new JSONBaseHeader();
		JSONBaseError error = null;
		PlainString content = new PlainString();
		HttpStatus httpStatus;

		if (id > 0 && id < Integer.MAX_VALUE) {
			header.setSuccess(true);
			content.setString("A Plain String with id " + id);
			httpStatus = HttpStatus.OK;
		} else {
			error = new JSONBaseError();

			header.setSuccess(false);
			error.setCode(HttpStatus.BAD_REQUEST.name());
			error.setMessage("ID out of range (must be between 1 and " + Integer.MAX_VALUE + ")");
			error.setReason("Number out of range");
			content = null;
			httpStatus = HttpStatus.BAD_REQUEST;
		}

		header.setError(error);
		result.setHeader(header);
		result.setContent(content);

		long time = System.currentTimeMillis() - start;
		header.setProcessTimeMillis(time);

		ResponseEntity<JSONBaseResponse<PlainString>> jsonResult = new ResponseEntity<>(result, httpStatus);

		LOG.logExecutionTime("/tests/{id}", time, Integer.toString(id), HttpUtil.getClientIp(request),
				httpStatus.name());

		return jsonResult;
	}

	@GetMapping("/tests/more/{id}")
	public ResponseEntity<JSONBaseResponse<PlainString>> testsMore(@PathVariable int id, HttpServletRequest request) {
		long start = System.currentTimeMillis();

		try {
			Thread.sleep((long) (Math.random() * 100));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		JSONBaseResponse<PlainString> result = new JSONBaseResponse<>();
		JSONBaseHeader header = new JSONBaseHeader();
		JSONBaseError error = null;
		PlainString content = new PlainString();
		HttpStatus httpStatus;

		if (id > 0 && id < Integer.MAX_VALUE) {
			header.setSuccess(true);
			content.setString("A More Plain String with id " + id);
			httpStatus = HttpStatus.OK;
		} else {
			error = new JSONBaseError();
			if (id % 3 == 0 || id % 7 == 0) {
				header.setSuccess(false);
				error.setCode(HttpStatus.INTERNAL_SERVER_ERROR.name());
				error.setMessage("Simulate server error");
				error.setReason("Simulate server error");
				content = null;
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			} else {
				header.setSuccess(false);
				error.setCode(HttpStatus.BAD_REQUEST.name());
				error.setMessage("ID out of range (must be between 1 and " + Integer.MAX_VALUE + ")");
				error.setReason("Number out of range");
				content = null;
				httpStatus = HttpStatus.BAD_REQUEST;
			}
		}

		header.setError(error);
		result.setHeader(header);
		result.setContent(content);

		long time = System.currentTimeMillis() - start;
		header.setProcessTimeMillis(time);

		ResponseEntity<JSONBaseResponse<PlainString>> jsonResult = new ResponseEntity<>(result, httpStatus);

		LOG.logExecutionTime("/tests/more/{id}", time, Integer.toString(id), HttpUtil.getClientIp(request),
				httpStatus.name());

		return jsonResult;
	}

	@GetMapping("/tests")
	public ResponseEntity<JSONBaseResponse<List<PlainString>>> tests() {
		long start = System.currentTimeMillis();

		JSONBaseResponse<List<PlainString>> result = new JSONBaseResponse<>();
		JSONBaseHeader header = new JSONBaseHeader();
		JSONBaseError error = null;
		List<PlainString> content = new ArrayList<>();

		try {
			Thread.sleep((long) (Math.random() * 200));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		for (int i = 0; i < 10; i++) {
			PlainString s = new PlainString();
			s.setString("A random string number : " + (long) (Math.random() * 1000));

			content.add(s);
		}

		header.setSuccess(true);

		header.setError(error);
		result.setHeader(header);
		result.setContent(content);

		long time = System.currentTimeMillis() - start;
		header.setProcessTimeMillis(time);

		ResponseEntity<JSONBaseResponse<List<PlainString>>> jsonResult = new ResponseEntity<>(result, HttpStatus.OK);

		LOG.logExecutionTime("/tests", time, HttpStatus.OK.name());

		return jsonResult;
	}

}
