package com.timpamungkas.oauth.client.json.base;

public class JSONBaseError {

	private String message;
	private String reason;
	private String code;

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public String getReason() {
		return reason;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
