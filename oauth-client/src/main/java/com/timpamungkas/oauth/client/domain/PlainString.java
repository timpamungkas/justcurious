package com.timpamungkas.oauth.client.domain;

public class PlainString {

	private String string;

	public String getString() {
		return string;
	}

	public void setString(String string) {
		this.string = string;
	}
	
}
