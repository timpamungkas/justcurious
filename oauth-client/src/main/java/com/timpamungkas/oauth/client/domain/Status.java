package com.timpamungkas.oauth.client.domain;

public enum Status {
	ACTIVE, INACTIVE
}
