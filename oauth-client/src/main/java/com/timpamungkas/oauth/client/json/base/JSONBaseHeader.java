package com.timpamungkas.oauth.client.json.base;

public class JSONBaseHeader {

	private long processTimeMillis;
	private boolean success;
	private JSONBaseError error;

	public JSONBaseError getError() {
		return error;
	}

	public long getProcessTimeMillis() {
		return processTimeMillis;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setError(JSONBaseError error) {
		this.error = error;
	}

	public void setProcessTimeMillis(long processTimeMillis) {
		this.processTimeMillis = processTimeMillis;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}
