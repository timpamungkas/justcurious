package com.timpamungkas.oauth.client.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;

public class HttpUtil {

	public static String responseToString(HttpResponse response) throws UnsupportedOperationException, IOException {
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}

		return result.toString();

	}

	public static JSONObject responseToJson(HttpResponse response)
			throws UnsupportedOperationException, IOException, JSONException {
		String responseString = responseToString(response);

		return new JSONObject(responseString);
	}

	public static String getClientIp(HttpServletRequest httpServletRequest) {
		String remoteAddr = "";

		if (httpServletRequest != null) {
			remoteAddr = httpServletRequest.getRemoteAddr();
			if (remoteAddr == null || remoteAddr.length() == 0) {
				remoteAddr = httpServletRequest.getHeader("X-FORWARDED-FOR");
			}
		}

		return remoteAddr;
	}

}
