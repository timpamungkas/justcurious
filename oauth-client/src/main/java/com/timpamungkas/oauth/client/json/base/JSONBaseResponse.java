package com.timpamungkas.oauth.client.json.base;

public class JSONBaseResponse<T> {

	private JSONBaseHeader header;

	private T content;

	public T getContent() {
		return content;
	}

	public JSONBaseHeader getHeader() {
		return header;
	}

	public void setContent(T content) {
		this.content = content;
	}

	public void setHeader(JSONBaseHeader header) {
		this.header = header;
	}

}
