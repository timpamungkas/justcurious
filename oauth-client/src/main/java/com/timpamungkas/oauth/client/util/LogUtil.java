package com.timpamungkas.oauth.client.util;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

public class LogUtil {

	public static final Marker MARKER_EXECUTION_TIME = MarkerFactory.getMarker("EXECUTION_TIME");
	@NotNull
	private Logger logger;

	public LogUtil(Logger logger) {
		if (logger == null) {
			throw new NullPointerException();
		}

		this.logger = logger;
	}

	/**
	 * By default, will send as "INFO" level with marker "EXECUTION_TIME"
	 */
	public void logExecutionTime(String identifier, long executiontimeMillis, String... additionalData) {
		if (identifier == null || identifier.length() == 0 || executiontimeMillis < 0) {
			throw new IllegalArgumentException("Identifier must not null AND execution time must >= 0");
		}

		StringBuilder sb = new StringBuilder(identifier);
		sb.append(" timeMillis:[" + executiontimeMillis + "] data:");

		for (int i = 0; i < additionalData.length; i++) {
			sb.append(additionalData[i]);
			if (i < additionalData.length - 1) {
				sb.append("||");

			}
		}

		this.logger.info(MARKER_EXECUTION_TIME, sb.toString());
	}

}
