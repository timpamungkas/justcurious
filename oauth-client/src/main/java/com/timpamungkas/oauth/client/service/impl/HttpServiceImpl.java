package com.timpamungkas.oauth.client.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.timpamungkas.oauth.client.domain.Patient;
import com.timpamungkas.oauth.client.service.HttpService;
import com.timpamungkas.oauth.client.util.HttpUtil;

@Service
public class HttpServiceImpl implements HttpService {

	private static final String CREDENTIAL = "Basic "
			+ Base64.getEncoder().encodeToString("acme:acmesecret".getBytes());

	public JSONObject getToken(String authCode) throws IOException, JSONException {
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost("http://localhost:8081/oauth/token");

		httpPost.setHeader("Authorization", CREDENTIAL);

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("client_id", "acme"));
		params.add(new BasicNameValuePair("redirect_uri", "http://127.0.0.1:8080/authCode"));
		params.add(new BasicNameValuePair("code", authCode));
		params.add(new BasicNameValuePair("grant_type", "authorization_code"));

		httpPost.setEntity(new UrlEncodedFormEntity(params));

		try (CloseableHttpResponse response = client.execute(httpPost)) {
			return HttpUtil.responseToJson(response);
		} finally {
			client.close();
		}
	}

	public List<Patient> getResource(String token) throws IOException, JSONException {
		CloseableHttpClient client = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet("http://localhost:8082/api/patients");

		httpGet.setHeader("Authorization", "bearer " + token);
		try (CloseableHttpResponse response = client.execute(httpGet)) {
			return responseParser(response);
		} finally {
			client.close();
		}
	}

	public List<Patient> responseParser(CloseableHttpResponse response) throws IOException, JSONException {
		List<Patient> patientList = new ArrayList<>();
		JSONArray jsonarray = new JSONArray(HttpUtil.responseToString(response));
		ObjectMapper mapper = new ObjectMapper();

		for (int i = 0; i < jsonarray.length(); i++) {
			JSONObject jsonobject = jsonarray.getJSONObject(i);
			Patient patient = mapper.readValue(jsonobject.toString(), Patient.class);
			patientList.add(patient);
		}

		return patientList;
	}

	public String getPublicInfo() throws IOException {
		CloseableHttpClient client = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet("http://localhost:8082/api/public/resource1");

		try (CloseableHttpResponse response = client.execute(httpGet)) {
			return HttpUtil.responseToString(response);
		} finally {
			client.close();
		}
	}

}
