package com.timpamungkas.oauth.client.domain;

public enum Gender {
	MALE, FEMALE
}
