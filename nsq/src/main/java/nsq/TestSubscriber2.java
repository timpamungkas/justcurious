package nsq;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.sproutsocial.nsq.Subscriber;

// Apparently, unable to have multiple processor for same message (in rabbit, this can be done using pubsub fanout exchange
public class TestSubscriber2 {

	private static final int NUM_THREAD = 1;
	private static ExecutorService executors = Executors.newFixedThreadPool(NUM_THREAD);

	public static void main(String[] args) {
		System.out.println("Start");

		Subscriber subscriber = new Subscriber("localhost");

		for (int i = 0; i < NUM_THREAD; i++) {
			executors.execute(new TestSubscriberRunnable2(subscriber));
		}
	}

}
