package nsq;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.sproutsocial.nsq.Subscriber;

public class TestSubscriber {

	private static final int NUM_THREAD = 1;
	private static ExecutorService executors = Executors.newFixedThreadPool(NUM_THREAD);

	public static void main(String[] args) {
		System.out.println("Start");

		Subscriber subscriber = new Subscriber("localhost");

		for (int i = 0; i < NUM_THREAD; i++) {
			executors.execute(new TestSubscriberRunnable(subscriber));
		}
	}

}
