package nsq;

import com.sproutsocial.nsq.Message;
import com.sproutsocial.nsq.Subscriber;

public class TestSubscriberRunnable implements Runnable {

	private Subscriber subscriber;

	public TestSubscriberRunnable(Subscriber subscriber) {
		this.subscriber = subscriber;
	}

	public static void handleMessage(Message msg) {
		try {
			byte[] data = msg.getData();

			String s = new String(data);
			System.out.println("[" + Thread.currentThread().getName() + "]" + " Received:" + s);

			if (s.equalsIgnoreCase("babi")) {
				throw new NullPointerException("NPE");
			}
			
			msg.finish();
		} catch (Exception e) {
			System.out.println(msg.getAttempts());
			msg.requeue();
		}
	}

	// public static void handleData(byte[] data) {
	// }

	@Override
	public void run() {
		subscriber.subscribe("test", "channel", TestSubscriberRunnable::handleMessage);
	}

}
