package com.timpamungkas.rxjava.mock;

public class MockBookDAO {

	private static MockBookDAO service = new MockBookDAO();

	public static MockBookDAO get() {
		return service;
	}

	public MockBook[] getAllMockBooks() {
		return produceMockBooks();
	}

	private MockBook[] produceMockBooks() {
		MockBook[] array = { MockBook.create("Java Microservice", "Refcardz"), MockBook.create("RX Java", "Article"),
				MockBook.create("IOT in Action", "Refcardz"), MockBook.create("Java8 in Action", "Refcardz"), };

		return array;
	}

}
