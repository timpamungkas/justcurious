package com.timpamungkas.rxjava.mock;

import io.reactivex.observables.GroupedObservable;

public class MockBookUI {

	private MockBookService mockBookService = new MockBookService();

	public void printAllBooks() {
		mockBookService.getAllBooks().subscribe(book -> System.out.println(book),
				error -> System.err.println("Error : " + error.getMessage()), () -> System.out.println("Complete"));
	}

	public void printAllBooksTitle() {
		mockBookService.getAllBooks().map(book -> book.getTitle()).subscribe(book -> System.out.println(book),
				error -> System.err.println("Error : " + error.getMessage()), () -> System.out.println("Complete"));
	}

	public void printGroupwiseBook() {
		mockBookService.getAllBooks().groupBy(book -> groupByName(book))
				.subscribe(group -> group.subscribe(book -> showDocs(group, book)));
	}

	private String groupByName(MockBook doc) {
		return doc.getTitle().contains("Java") ? "JAVA" : "IOT";
	}

	private void showDocs(GroupedObservable<String, MockBook> observable, MockBook currentBook) {
		if ("JAVA".equalsIgnoreCase(observable.getKey().toString())) {
			System.out.println("JAVA::" + currentBook);
		} else {
			System.out.println("IOT::" + currentBook);
		}
	}

	public static void main(String[] args) {
		MockBookUI UI = new MockBookUI();
		UI.printAllBooks();
		System.out.println("\n");
		UI.printGroupwiseBook();
	}

}
