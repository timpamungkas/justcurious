package com.timpamungkas.rxjava.mock;

import io.reactivex.Observable;

public class MockBookService {

	public Observable<MockBook> getAllBooks() {
		return Observable.fromArray(MockBookDAO.get().getAllMockBooks());
	}

}
