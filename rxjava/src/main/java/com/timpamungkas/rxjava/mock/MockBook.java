package com.timpamungkas.rxjava.mock;

public class MockBook {

	public static MockBook create(String title, String genre) {
		MockBook mockBook = new MockBook();
		mockBook.setTitle(title);
		mockBook.setGenre(genre);

		return mockBook;
	}
	
	private String genre;

	private String title;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MockBook other = (MockBook) obj;
		if (genre == null) {
			if (other.genre != null)
				return false;
		} else if (!genre.equals(other.genre))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	public String getGenre() {
		return genre;
	}

	public String getTitle() {
		return title;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((genre == null) ? 0 : genre.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "MockBook [genre=" + genre + ", title=" + title + "]";
	}

}
