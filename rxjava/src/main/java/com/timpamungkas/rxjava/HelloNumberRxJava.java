package com.timpamungkas.rxjava;

import io.reactivex.Observable;

public class HelloNumberRxJava {

	public static void main(String[] args) {
		Observable.range(1, 10).subscribe(n -> System.out.println("Number is " + n),
				e -> System.err.println("Error is " + e.getMessage()), () -> System.out.println("Complete"));
	}

}
