package com.timpamungkas.rxjava;

import io.reactivex.Flowable;

public class HelloRxJava {

	public static void main(String[] args) {
		Flowable.just("Timotius", "Pamungkas").subscribe(System.out::println);
	}

}
