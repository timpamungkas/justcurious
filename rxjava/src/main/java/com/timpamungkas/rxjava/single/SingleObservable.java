package com.timpamungkas.rxjava.single;

import java.util.concurrent.Callable;

import io.reactivex.Observable;

public class SingleObservable {

	public static void main(String[] args) {
		Observable.fromCallable(new Callable<String>() {

			@Override
			public String call() throws Exception {
				Thread.sleep(3000);
				return "A string";
			}
		}).subscribe(System.out::println);
		
		System.out.println("Different string");
	}

}
