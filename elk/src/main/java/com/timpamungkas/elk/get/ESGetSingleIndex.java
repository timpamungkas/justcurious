package com.timpamungkas.elk.get;

import org.apache.http.HttpHost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.rest.RestStatus;

import com.timpamungkas.elk.constants.ElkConstants;

public class ESGetSingleIndex {

	private static final Logger log = LogManager.getLogger(ESGetSingleIndex.class);

	public static void main(String[] args) {
		try (RestHighLevelClient client = new RestHighLevelClient(
				RestClient.builder(new HttpHost(ElkConstants.ES_HOST, ElkConstants.ES_PORT)))) {
			String id = "Ce0UFGMBliRdePMYCTw9";
			GetRequest request = new GetRequest(ElkConstants.INDEX_DUMMY_NAME, ElkConstants.INDEX_DUMMY_TYPE, id);

			GetResponse response = client.get(request);

			if (response.isExists()) {
				log.info(response);
			} else {
				log.warn("No data found for id : " + id);
			}
		} catch (ElasticsearchException e) {
			if (e.status() == RestStatus.NOT_FOUND) {
				log.error("Index not found");
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

}
