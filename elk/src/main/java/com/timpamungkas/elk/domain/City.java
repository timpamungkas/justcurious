package com.timpamungkas.elk.domain;

public class City {

	private String country;
	private double lat;
	private double lng;
	private String name;
	
	public String getCountry() {
		return country;
	}
	public double getLat() {
		return lat;
	}
	public double getLng() {
		return lng;
	}
	public String getName() {
		return name;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	public void setName(String name) {
		this.name = name;
	}

}
