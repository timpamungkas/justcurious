package com.timpamungkas.elk.index;

import org.apache.http.HttpHost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.admin.indices.open.OpenIndexRequest;
import org.elasticsearch.action.admin.indices.open.OpenIndexResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;

import com.timpamungkas.elk.constants.ElkConstants;

/**
 * Refer to <a href=
 * "https://www.elastic.co/guide/en/elasticsearch/reference/6.2/indices-open-close.html">elasticsearch
 * document</a>.
 * 
 * @author timpamungkas
 *
 */
public class ESOpenIndexAsync {

	private static final Logger log = LogManager.getLogger(ESOpenIndexAsync.class);

	public static void main(String[] args) {
		try (RestHighLevelClient client = new RestHighLevelClient(
				RestClient.builder(new HttpHost(ElkConstants.ES_HOST, ElkConstants.ES_PORT)))) {
			OpenIndexRequest openIndexRequest = new OpenIndexRequest(ElkConstants.INDEX_DUMMY_NAME);
			openIndexRequest.timeout(TimeValue.timeValueSeconds(30));

			ActionListener<OpenIndexResponse> listener = new ActionListener<OpenIndexResponse>() {

				@Override
				public void onResponse(OpenIndexResponse response) {
					log.info("Open OK : " + response.isAcknowledged());
				}

				@Override
				public void onFailure(Exception e) {
					log.error("Open not OK : " + e.getMessage());
				}
			};
			
			client.indices().openAsync(openIndexRequest, listener);

			Thread.sleep(10000);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

}
