package com.timpamungkas.elk.index;

import org.apache.http.HttpHost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.admin.indices.close.CloseIndexRequest;
import org.elasticsearch.action.admin.indices.close.CloseIndexResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;

import com.timpamungkas.elk.constants.ElkConstants;

/**
 * Refer to <a href=
 * "https://www.elastic.co/guide/en/elasticsearch/reference/6.2/indices-open-close.html">elasticsearch
 * document</a>.
 * 
 * @author timpamungkas
 *
 */
public class ESCloseIndexAsync {

	private static final Logger log = LogManager.getLogger(ESCloseIndexAsync.class);

	public static void main(String[] args) {
		try (RestHighLevelClient client = new RestHighLevelClient(
				RestClient.builder(new HttpHost(ElkConstants.ES_HOST, ElkConstants.ES_PORT)))) {
			CloseIndexRequest closeIndexRequest = new CloseIndexRequest(ElkConstants.INDEX_DUMMY_NAME);
			closeIndexRequest.timeout(TimeValue.timeValueSeconds(30));

			ActionListener<CloseIndexResponse> listener = new ActionListener<CloseIndexResponse>() {

				@Override
				public void onResponse(CloseIndexResponse response) {
					log.info("Close OK : " + response.isAcknowledged());
				}

				@Override
				public void onFailure(Exception e) {
					log.error("Close not OK : " + e.getMessage());
				}
			};

			client.indices().closeAsync(closeIndexRequest, listener);

			Thread.sleep(10000);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

}
