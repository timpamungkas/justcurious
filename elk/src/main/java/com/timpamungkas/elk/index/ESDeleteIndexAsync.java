package com.timpamungkas.elk.index;

import org.apache.http.HttpHost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;

import com.timpamungkas.elk.constants.ElkConstants;

public class ESDeleteIndexAsync {

	private static final Logger log = LogManager.getLogger(ESDeleteIndexAsync.class);

	public static void main(String[] args) {
		try (RestHighLevelClient client = new RestHighLevelClient(
				RestClient.builder(new HttpHost(ElkConstants.ES_HOST, ElkConstants.ES_PORT)))) {
			DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest(ElkConstants.INDEX_DUMMY_NAME);
			deleteIndexRequest.timeout(TimeValue.timeValueSeconds(30));

			ActionListener<DeleteIndexResponse> listener = new ActionListener<DeleteIndexResponse>() {

				@Override
				public void onResponse(DeleteIndexResponse response) {
					log.info("Delete OK : " + response.isAcknowledged());
				}

				@Override
				public void onFailure(Exception e) {
					log.error("Delete not OK : " + e.getMessage());
				}
			};
			
			client.indices().deleteAsync(deleteIndexRequest, listener);

			Thread.sleep(10000);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

}
