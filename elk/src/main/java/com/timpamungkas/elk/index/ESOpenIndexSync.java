package com.timpamungkas.elk.index;

import org.apache.http.HttpHost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.admin.indices.open.OpenIndexRequest;
import org.elasticsearch.action.admin.indices.open.OpenIndexResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;

import com.timpamungkas.elk.constants.ElkConstants;

/**
 * Refer to <a href=
 * "https://www.elastic.co/guide/en/elasticsearch/reference/6.2/indices-open-close.html">elasticsearch
 * document</a>.
 * 
 * @author timpamungkas
 *
 */
public class ESOpenIndexSync {

	private static final Logger log = LogManager.getLogger(ESOpenIndexSync.class);

	public static void main(String[] args) {
		try (RestHighLevelClient client = new RestHighLevelClient(
				RestClient.builder(new HttpHost(ElkConstants.ES_HOST, ElkConstants.ES_PORT)))) {

			OpenIndexRequest request = new OpenIndexRequest(ElkConstants.INDEX_DUMMY_NAME);
			request.timeout(TimeValue.timeValueSeconds(30));

			OpenIndexResponse response = client.indices().open(request);

			log.info(response.isAcknowledged());
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

}
