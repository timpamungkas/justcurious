package com.timpamungkas.elk.index;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpHost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.timpamungkas.elk.constants.ElkConstants;
import com.timpamungkas.elk.domain.City;
import com.timpamungkas.elk.util.ESUtil;

/**
 * Put a document to index and mke it searchable. See <a href=
 * "https://www.elastic.co/guide/en/elasticsearch/client/java-api/current/java-docs-index.html">here</a>.
 * 
 * @author timpamungkas
 *
 */
public class ESPutIndex {

	private static final Logger log = LogManager.getLogger();

	public static void main(String[] args) {
		try (RestHighLevelClient client = new RestHighLevelClient(
				RestClient.builder(new HttpHost(ElkConstants.ES_HOST, ElkConstants.ES_PORT)))) {
			ESUtil esUtil = new ESUtil(client);
			String id;

			esUtil.recreateDummyIndex();

			// id = putJsonStringSample(client);
			// log.info("Index ID : " + id);

			// id = putJsonMapSample(client);
			// log.info("Index ID : " + id);

			id = putJsonURLSample(client);
			log.info("Index ID : " + id);
		} catch (Exception e) {
			log.error("Error put index: " + e.getMessage());
		}
	}

	private static String putJsonMapSample(RestHighLevelClient client) throws IOException {
		Map<String, String> jsonMap = new HashMap<>();

		jsonMap.put("country", "AD");
		jsonMap.put("name", "Ordino");
		jsonMap.put("lat", "42.55623");
		jsonMap.put("lng", "1.53319");

		IndexRequest request = new IndexRequest(ElkConstants.INDEX_DUMMY_NAME, ElkConstants.INDEX_DUMMY_TYPE);
		request.source(jsonMap);
		request.timeout("30s");

		return client.index(request).getId();
	}

	private static String putJsonStringSample(RestHighLevelClient client) throws IOException {
		String jsonString = "{" + "    \"country\": \"AD\"," + "    \"name\": \"Pas de la Casa\","
				+ "    \"lat\": \"42.54277\"," + "    \"lng\": \"1.73361\"" + "  }";

		IndexRequest request = new IndexRequest(ElkConstants.INDEX_DUMMY_NAME, ElkConstants.INDEX_DUMMY_TYPE);
		request.source(jsonString, XContentType.JSON);
		request.timeout("30s");

		return client.index(request).getId();
	}

	private static String putJsonURLSample(RestHighLevelClient client) throws IOException {
		URL url = new URL("https://raw.githubusercontent.com/lutangar/cities.json/master/cities.json");
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		List<City> cities = objectMapper.readValue(url, new TypeReference<List<City>>() {
		});

		BulkRequest bulkRequest = new BulkRequest();
		long totalMillis = 0;

		for (int i = 0; i < cities.size(); i++) {
			// log.debug(i + " of " + cities.size());
			byte[] json = objectMapper.writeValueAsBytes(cities.get(i));

			IndexRequest request = new IndexRequest(ElkConstants.INDEX_DUMMY_NAME, ElkConstants.INDEX_DUMMY_TYPE);
			request.source(json, XContentType.JSON);
			request.timeout(TimeValue.timeValueMinutes(1));

			bulkRequest.add(request);

			// WARNING : Wrong, primitive implementation. See ESBulkIndex.java for correct implementation
			if (i > 0 && i % 5000 == 0) {
				long millis = client.bulk(bulkRequest).getTook().getMillis();
				totalMillis += millis;
				log.info(i + " Executed in : " + millis + "ms");

				bulkRequest = new BulkRequest();
			}
		}

		// last sweep
		totalMillis += client.bulk(bulkRequest).getTook().getMillis();

		return "Total execution time millis : " + totalMillis;
	}

}