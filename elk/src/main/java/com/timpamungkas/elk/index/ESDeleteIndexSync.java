package com.timpamungkas.elk.index;

import org.apache.http.HttpHost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;

import com.timpamungkas.elk.constants.ElkConstants;

public class ESDeleteIndexSync {

	private static final Logger log = LogManager.getLogger(ESDeleteIndexSync.class);

	public static void main(String[] args) {
		try (RestHighLevelClient client = new RestHighLevelClient(
				RestClient.builder(new HttpHost(ElkConstants.ES_HOST, ElkConstants.ES_PORT)))) {
			DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest(ElkConstants.INDEX_DUMMY_NAME);
			deleteIndexRequest.timeout(TimeValue.timeValueSeconds(30));

			DeleteIndexResponse deleteIndexResponse = client.indices().delete(deleteIndexRequest);

			log.info("Acknowledged : " + deleteIndexResponse.isAcknowledged());
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

}
