package com.timpamungkas.elk.index;

import org.apache.http.HttpHost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.admin.indices.close.CloseIndexRequest;
import org.elasticsearch.action.admin.indices.close.CloseIndexResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;

import com.timpamungkas.elk.constants.ElkConstants;

/**
 * Refer to <a href=
 * "https://www.elastic.co/guide/en/elasticsearch/reference/6.2/indices-Close-close.html">elasticsearch
 * document</a>.
 * 
 * @author timpamungkas
 *
 */
public class ESCloseIndexSync {

	private static final Logger log = LogManager.getLogger(ESCloseIndexSync.class);

	public static void main(String[] args) {
		try (RestHighLevelClient client = new RestHighLevelClient(
				RestClient.builder(new HttpHost(ElkConstants.ES_HOST, ElkConstants.ES_PORT)))) {

			CloseIndexRequest request = new CloseIndexRequest(ElkConstants.INDEX_DUMMY_NAME);
			request.timeout(TimeValue.timeValueSeconds(30));

			CloseIndexResponse response = client.indices().close(request);

			log.info(response.isAcknowledged());
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

}
