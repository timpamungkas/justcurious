package com.timpamungkas.elk.index;

import java.net.InetAddress;
import java.net.URL;
import java.util.List;

import org.apache.http.HttpHost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkProcessor.Listener;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.timpamungkas.elk.constants.ElkConstants;
import com.timpamungkas.elk.domain.City;
import com.timpamungkas.elk.util.ESUtil;

/**
 * Put a document to index and mke it searchable. See <a href=
 * "https://www.elastic.co/guide/en/elasticsearch/client/java-api/current/java-docs-index.html">here</a>.
 * 
 * @author timpamungkas
 *
 */
public class ESBulkIndex {

	private static final Logger log = LogManager.getLogger();

	public static void main(String[] args) {
		BulkProcessor bulkProcessor = null;

		try (TransportClient client = new PreBuiltTransportClient(Settings.EMPTY)
				.addTransportAddress(new TransportAddress(InetAddress.getByName(ElkConstants.ES_HOST), 9300));
				RestHighLevelClient restHighLevelClient = new RestHighLevelClient(
						RestClient.builder(new HttpHost(ElkConstants.ES_HOST, ElkConstants.ES_PORT)))) {
			ESUtil esUtil = new ESUtil(restHighLevelClient);

			log.debug("Recreate index");

			esUtil.recreateDummyIndex();

			log.debug("Getting json data");

			URL url = new URL("https://raw.githubusercontent.com/lutangar/cities.json/master/cities.json");
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			List<City> cities = objectMapper.readValue(url, new TypeReference<List<City>>() {
			});

			log.debug("Getting json data : DONE");

			Listener listener = new Listener() {

				@Override
				public void beforeBulk(long executionId, BulkRequest request) {
					log.debug("beforeBulk");
				}

				@Override
				public void afterBulk(long executionId, BulkRequest request, Throwable failure) {
					log.debug("afterBulk, failure");
				}

				@Override
				public void afterBulk(long executionId, BulkRequest request, BulkResponse response) {
					log.debug("afterBulk, success");
				}
			};

			BulkProcessor.Builder bulkBuilder = BulkProcessor.builder(client, listener);
			bulkProcessor = bulkBuilder.setBulkActions(1000).setBulkSize(new ByteSizeValue(2, ByteSizeUnit.MB))
					.setConcurrentRequests(1).setFlushInterval(TimeValue.timeValueMinutes(5)).build();
			// bulkProcessor.awaitClose(1, TimeUnit.MINUTES);

			log.debug("BulkProcessor : " + bulkProcessor + " start processing ");

			for (City city : cities) {
				byte[] json = objectMapper.writeValueAsBytes(city);
				IndexRequest request = new IndexRequest(ElkConstants.INDEX_DUMMY_NAME, ElkConstants.INDEX_DUMMY_TYPE);
				request.source(json, XContentType.JSON);

				bulkProcessor.add(request);
			}

			// flush last items
			bulkProcessor.flush();
		} catch (Exception e) {
			log.error("Error put index: " + e.getMessage());
		} finally {
			if (bulkProcessor != null) {
				log.debug("Closing BulkProcessor");
				bulkProcessor.close();
			}
		}
	}

}