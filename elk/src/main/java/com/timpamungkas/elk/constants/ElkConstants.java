package com.timpamungkas.elk.constants;

public interface ElkConstants {

	String ES_HOST = "localhost";

	int ES_PORT = 9200;

	String INDEX_DUMMY_NAME = "timpamungkas";

	String INDEX_DUMMY_TYPE = "dummy";

}
