package com.timpamungkas.elk.util;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;

import com.timpamungkas.elk.constants.ElkConstants;

public class ESUtil {

	private static final Logger log = LogManager.getLogger();
	private RestHighLevelClient client;

	public ESUtil(RestHighLevelClient client) {
		this.client = client;
	}

	public void recreateDummyIndex() throws IOException {
		try {
			DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest(ElkConstants.INDEX_DUMMY_NAME);
			deleteIndexRequest.timeout(TimeValue.timeValueSeconds(30));

			client.indices().delete(deleteIndexRequest);
		} catch (ElasticsearchException e) {
			// no index found
			log.warn(e.getMessage());
		}

		CreateIndexRequest createIndexRequest = new CreateIndexRequest(ElkConstants.INDEX_DUMMY_NAME);
		createIndexRequest.timeout(TimeValue.timeValueSeconds(30));

		client.indices().create(createIndexRequest);
	}

}
