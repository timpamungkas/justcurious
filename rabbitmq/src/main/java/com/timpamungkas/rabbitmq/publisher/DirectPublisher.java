package com.timpamungkas.rabbitmq.publisher;

import java.util.concurrent.ThreadLocalRandom;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.timpamungkas.rabbitmq.constants.RabbitMqConnectionConstants;

public class DirectPublisher {

	public final static String EXCHANGE_NAME = "ex.direct";

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();

		factory.setHost(RabbitMqConnectionConstants.HOST);
		factory.setPort(RabbitMqConnectionConstants.PORT);
		factory.setUsername(RabbitMqConnectionConstants.USERNAME);
		factory.setPassword(RabbitMqConnectionConstants.PASSWORD);
		factory.setHandshakeTimeout(RabbitMqConnectionConstants.HANDSHAKE_TIMEOUT);

		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
		// channel.queueDeclare(EXCHANGE_NAME, false, false, false, null);

		for (int i = 1; i <= 40; i++) {
			int randomNum = 3;
			String directKey;
			if (i % 2 == 0) {
				randomNum = 5;
				directKey = "evenNumber";
			} else {
				randomNum = ThreadLocalRandom.current().nextInt(1, 4);
				directKey = "oddNumber";
			}

			String message = "Hello World " + i;

			for (int j = 1; j <= randomNum; j++) {
				message += '.';
			}

			channel.basicPublish(DirectPublisher.EXCHANGE_NAME, directKey, null, message.getBytes("UTF-8"));
			System.out.println(" [x] Sent '" + message + "'");
		}

		channel.close();
		connection.close();
	}

}
