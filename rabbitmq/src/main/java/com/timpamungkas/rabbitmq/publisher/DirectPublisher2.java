package com.timpamungkas.rabbitmq.publisher;

import java.util.HashMap;
import java.util.Map;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.timpamungkas.rabbitmq.constants.RabbitMqConnectionConstants;

public class DirectPublisher2 {

	public final static String EXCHANGE_NAME = "x.guideline2.retry";

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();

		factory.setHost(RabbitMqConnectionConstants.HOST);
		factory.setPort(RabbitMqConnectionConstants.PORT);
		factory.setUsername(RabbitMqConnectionConstants.USERNAME);
		factory.setPassword(RabbitMqConnectionConstants.PASSWORD);
		factory.setHandshakeTimeout(RabbitMqConnectionConstants.HANDSHAKE_TIMEOUT);

		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		Map<String, Object> arguments = new HashMap<>();
//		arguments.put("x-dead-letter-exchange", "x.guideline2.dlx");
//		arguments.put("x-dead-letter-routing-key", "q.guideline2.one");

		channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT, true, false, arguments);

		for (int i = 1; i <= 1; i++) {
			int randomNum = 3;
			String directKey = "q.guideline2.one";

			String message = "Hello Babi " + i;

			for (int j = 1; j <= randomNum; j++) {
				message += '.';
			}

			channel.basicPublish(DirectPublisher2.EXCHANGE_NAME, directKey, null, message.getBytes("UTF-8"));
			System.out.println(" [x] Sent '" + message + "'");
		}

		channel.close();
		connection.close();
	}

}
