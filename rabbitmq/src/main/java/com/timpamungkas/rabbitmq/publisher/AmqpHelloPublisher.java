package com.timpamungkas.rabbitmq.publisher;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.timpamungkas.rabbitmq.entity.PlainString;

@Component
public class AmqpHelloPublisher {

	@Autowired
	private RabbitTemplate rabbitTemplate;

	public void send() throws InterruptedException {
		for (int i = 0; i < 2; i++) {
			PlainString s = new PlainString("Simple string message " + i);

			rabbitTemplate.convertAndSend("hello", s);
		}
	}

}
