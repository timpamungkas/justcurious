package com.timpamungkas.rabbitmq.publisher;

import java.util.Date;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.timpamungkas.rabbitmq.entity.mybatis.model.Person;

@Component
public class AmqpPersonPublisher {

	@Autowired
	private RabbitTemplate rabbitTemplate;

	public void send() throws InterruptedException {
		for (int i = 0; i < 2; i++) {
			Person p = new Person();
			p.setBirthDate(new Date());
			p.setFirstName("First name " + i);
			p.setLastName("Last name " + i);

			rabbitTemplate.convertAndSend("x.guideline.work", "q.guideline.payment", p);
		}
	}

}
