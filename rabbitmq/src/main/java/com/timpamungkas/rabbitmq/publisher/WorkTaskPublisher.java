package com.timpamungkas.rabbitmq.publisher;

import java.util.concurrent.ThreadLocalRandom;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.timpamungkas.rabbitmq.constants.RabbitMqConnectionConstants;

public class WorkTaskPublisher {

	private final static String QUEUE_NAME = "work_task";

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();

		factory.setHost(RabbitMqConnectionConstants.HOST);
		factory.setPort(RabbitMqConnectionConstants.PORT);
		factory.setUsername(RabbitMqConnectionConstants.USERNAME);
		factory.setPassword(RabbitMqConnectionConstants.PASSWORD);
		factory.setHandshakeTimeout(RabbitMqConnectionConstants.HANDSHAKE_TIMEOUT);

		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		boolean durable = true;
		channel.queueDeclare(QUEUE_NAME, durable, false, false, null);

		for (int i = 1; i <= 1000; i++) {
			int randomNum = 5;
			if (i % 2 == 0) {
				randomNum = 5;
			} else {
				randomNum = ThreadLocalRandom.current().nextInt(1, 4);
			}

			String message = "Hello World " + i;

			for (int j = 1; j <= randomNum; j++) {
				message += '.';
			}

			channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
			System.out.println(" [x] Sent '" + message + "'");
		}

		channel.close();
		connection.close();
	}
}