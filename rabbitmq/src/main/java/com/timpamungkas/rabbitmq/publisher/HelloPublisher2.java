package com.timpamungkas.rabbitmq.publisher;

import java.util.HashMap;
import java.util.Map;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import com.timpamungkas.rabbitmq.constants.RabbitMqConnectionConstants;

public class HelloPublisher2 {

	private final static String QUEUE_NAME = "q.guideline2.one.dlx";

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();

		factory.setHost(RabbitMqConnectionConstants.HOST);
		factory.setPort(RabbitMqConnectionConstants.PORT);
		factory.setUsername(RabbitMqConnectionConstants.USERNAME);
		factory.setPassword(RabbitMqConnectionConstants.PASSWORD);
		factory.setHandshakeTimeout(RabbitMqConnectionConstants.HANDSHAKE_TIMEOUT);

		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		boolean durable = true;
		boolean exclusive = false;
		boolean autodelete = false;
		Map<String, Object> arguments = new HashMap<>();
		arguments.put("x-dead-letter-exchange", "x.guideline2.retry");
		arguments.put("x-dead-letter-routing-key", "q.guideline2.one");
		arguments.put("x-message-ttl", 30000);

		channel.queueDeclare(QUEUE_NAME, durable, exclusive, autodelete, arguments);

		for (int i = 1; i <= 1; i++) {
			String message = "Hello World! " + i;
			channel.basicPublish("", QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes("UTF-8"));
			System.out.println(" [x] Sent '" + message + "'");
		}

		channel.close();
		connection.close();
	}
}