package com.timpamungkas.rabbitmq.publisher;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.ThreadLocalRandom;

import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.timpamungkas.rabbitmq.constants.RabbitMqConnectionConstants;
import com.timpamungkas.rabbitmq.entity.mybatis.model.Person;

public class FanoutGuideline2JsonPublisher {

	public final static String EXCHANGE_NAME = "x.guideline2.publish";

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();

		factory.setHost(RabbitMqConnectionConstants.HOST);
		factory.setPort(RabbitMqConnectionConstants.PORT);
		factory.setUsername(RabbitMqConnectionConstants.USERNAME);
		factory.setPassword(RabbitMqConnectionConstants.PASSWORD);
		factory.setHandshakeTimeout(RabbitMqConnectionConstants.HANDSHAKE_TIMEOUT);

		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		boolean durable = true;
		channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT, durable);

		ObjectMapper objectMapper = new ObjectMapper();

		for (int i = 1; i <= 1; i++) {
			Person person = new Person();
			DateTime dateTime = new DateTime();
			person.setFirstName("[R] First name " + i);
			person.setLastName("[R] Last name " + i);
			person.setBirthDate(dateTime.minusDays(ThreadLocalRandom.current().nextInt(1, 365 * 30)).toDate());

			String jsonString = objectMapper.writeValueAsString(person);

			channel.basicPublish(FanoutGuideline2JsonPublisher.EXCHANGE_NAME, "", null,
					jsonString.getBytes(StandardCharsets.UTF_8.name()));
			System.out.println(" [x] Sent '" + jsonString + "'");
		}

		channel.close();
		connection.close();
	}

}
