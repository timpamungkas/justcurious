package com.timpamungkas.rabbitmq.consumer;

import java.io.IOException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.timpamungkas.rabbitmq.constants.RabbitMqConnectionConstants;
import com.timpamungkas.rabbitmq.publisher.FanoutPublisher;

public class FanoutSubscriber {

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();

		factory.setHost(RabbitMqConnectionConstants.HOST);
		factory.setPort(RabbitMqConnectionConstants.PORT);
		factory.setUsername(RabbitMqConnectionConstants.USERNAME);
		factory.setPassword(RabbitMqConnectionConstants.PASSWORD);
		factory.setHandshakeTimeout(RabbitMqConnectionConstants.HANDSHAKE_TIMEOUT);

		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		channel.exchangeDeclare(FanoutPublisher.EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
		String queueName = channel.queueDeclare().getQueue();
		channel.queueBind(queueName, FanoutPublisher.EXCHANGE_NAME, "");

		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

		Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				String message = new String(body, "UTF-8");
				try {
					doWork(message);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					System.out.println(" [x] Done " + message);
					// channel.basicAck(envelope.getDeliveryTag(), false);
				}
			}

			private void doWork(String message) throws InterruptedException {
				for (char ch : message.toCharArray()) {
					if (ch == '.')
						Thread.sleep(1000);
				}
			}
		};
		channel.basicConsume(queueName, true, consumer);
	}
}