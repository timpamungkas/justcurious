package com.timpamungkas.rabbitmq.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Service;

import com.timpamungkas.rabbitmq.entity.mybatis.model.Person;

@Service
public class AmqpPersonSubscriber {

	@RabbitListener(queues = "q.guideline.payment.work")
	public void Receive(Person in, MessageHeaders header) {
		// TODO delayed retry mechanism
		System.out.println("Person is : " + in + ", " + header);
	}

}
