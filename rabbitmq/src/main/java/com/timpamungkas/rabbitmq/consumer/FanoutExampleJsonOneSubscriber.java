package com.timpamungkas.rabbitmq.consumer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.timpamungkas.rabbitmq.constants.RabbitMqConnectionConstants;
import com.timpamungkas.rabbitmq.entity.mybatis.model.Person;

/**
 * Assuming no error on message processing (on doWork()).
 * @author timotiusps
 */
public class FanoutExampleJsonOneSubscriber {

	private final static String QUEUE_NAME = "q.guideline.booking.work";
	private static ExecutorService executorService = Executors.newFixedThreadPool(2);

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();

		factory.setHost(RabbitMqConnectionConstants.HOST);
		factory.setPort(RabbitMqConnectionConstants.PORT);
		factory.setUsername(RabbitMqConnectionConstants.USERNAME);
		factory.setPassword(RabbitMqConnectionConstants.PASSWORD);
		factory.setHandshakeTimeout(RabbitMqConnectionConstants.HANDSHAKE_TIMEOUT);

		Connection connection = factory.newConnection();
		System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

		for (int i = 0; i < 2; i++) {
			executorService.submit(new FanoutExampleJsonOneRunnable(connection, QUEUE_NAME, Integer.toString(i)));
		}

		executorService.shutdown();
	}
}

class FanoutExampleJsonOneRunnable implements Runnable {

	private Channel channel;
	private String queueName;
	private String runnableName;
	private ObjectMapper objectMapper = new ObjectMapper();

	public FanoutExampleJsonOneRunnable(Connection connection, String queueName, String runnableName)
			throws IOException {
		this.queueName = queueName;
		this.runnableName = runnableName;
		Channel channel = connection.createChannel();

		channel.basicQos(1);
		boolean durable = true;
		boolean exclusive = false;
		boolean autodelete = false;
		Map<String, Object> arguments = new HashMap<>();
		arguments.put("x-dead-letter-exchange", "x.guideline.wait");
		arguments.put("x-dead-letter-routing-key", "q.guideline.booking");

		channel.queueDeclare(queueName, durable, exclusive, autodelete, arguments);

		this.channel = channel;
	}

	@Override
	public void run() {
		Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				String message = new String(body, "UTF-8");
				try {
					doWork(message);
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					channel.basicAck(envelope.getDeliveryTag(), false);
				}
			}

			private void doWork(String message)
					throws InterruptedException, JsonParseException, JsonMappingException, IOException {
				// convert back to person
				Person person = objectMapper.readValue(message, Person.class);

				Thread.sleep(2000);
				
				System.out.println(this.getClass().getName() + " " + "[runnable "
						+ FanoutExampleJsonOneRunnable.this.runnableName + "] Person is : " + person);
			}
		};
		boolean autoAck = false;

		try {
			channel.basicConsume(queueName, autoAck, consumer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}