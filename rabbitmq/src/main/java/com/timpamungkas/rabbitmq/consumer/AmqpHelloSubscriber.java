package com.timpamungkas.rabbitmq.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.timpamungkas.rabbitmq.entity.PlainString;
import com.timpamungkas.rabbitmq.entity.mybatis.mapper.HelloMapper;
import com.timpamungkas.rabbitmq.entity.mybatis.model.Hello;

@Service
public class AmqpHelloSubscriber {

	@Autowired
	private HelloMapper helloMapper;

	@RabbitListener(queues = "hello")
	public void Receive(PlainString in) {
		System.out.println("String is : " + in + ", " + helloMapper);

		Hello hello = new Hello();
		hello.setRawMessage(in.getString());

		helloMapper.insert(hello);
	}

}
