package com.timpamungkas.rabbitmq.consumer;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.timpamungkas.rabbitmq.constants.RabbitMqConnectionConstants;
import com.timpamungkas.rabbitmq.entity.mybatis.model.Person;
import com.timpamungkas.rabbitmq.util.RabbitMqHeader;

/**
 * Reject message on error, send back to dlx.
 */
public class FanoutGuideline2OneSubscriber {

	static final String WORK_QUEUE_NAME = "q.guideline2.one";
	static final String WAIT_EXCHANGE_ROUTING_KEY = WORK_QUEUE_NAME;
	static final String WAIT_EXCHANGE_NAME = "x.guideline2.wait";
	static final String DEAD_EXCHANGE_ROUTING_KEY = WORK_QUEUE_NAME;
	static final String DEAD_EXCHANGE_NAME = "x.guideline2.dead";

	private static ExecutorService executorService = Executors.newFixedThreadPool(2);
	static final int MAX_RETRY = 3;

	public static void main(String[] argv) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();

		factory.setHost(RabbitMqConnectionConstants.HOST);
		factory.setPort(RabbitMqConnectionConstants.PORT);
		factory.setUsername(RabbitMqConnectionConstants.USERNAME);
		factory.setPassword(RabbitMqConnectionConstants.PASSWORD);
		factory.setHandshakeTimeout(RabbitMqConnectionConstants.HANDSHAKE_TIMEOUT);

		Connection connection = factory.newConnection();
		System.out.println(" [*] Waiting for messages....");

		for (int i = 0; i < 1; i++) {
			executorService.submit(new FanoutGuideline2OneRunnable(connection, WORK_QUEUE_NAME, Integer.toString(i)));
		}

		executorService.shutdown();
	}
}

class FanoutGuideline2OneRunnable implements Runnable {

	private Channel channel;
	private String queueName;
	private String runnableName;
	private ObjectMapper objectMapper = new ObjectMapper();

	public FanoutGuideline2OneRunnable(Connection connection, String queueName, String runnableName)
			throws IOException {
		this.queueName = queueName;
		this.runnableName = runnableName;
		Channel channel = connection.createChannel();

		channel.basicQos(1);
		boolean durable = true;
		boolean exclusive = false;
		boolean autodelete = false;
		Map<String, Object> arguments = new HashMap<>();
		arguments.put("x-dead-letter-exchange", FanoutGuideline2OneSubscriber.WAIT_EXCHANGE_NAME);
		arguments.put("x-dead-letter-routing-key", FanoutGuideline2OneSubscriber.WAIT_EXCHANGE_ROUTING_KEY);

		channel.queueDeclare(queueName, durable, exclusive, autodelete, arguments);

		this.channel = channel;
	}

	@Override
	public void run() {
		Consumer consumer = new DefaultConsumer(channel) {
			@Override
			public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties,
					byte[] body) throws IOException {
				String message = new String(body, StandardCharsets.UTF_8.name());
				try {
					doWork(message);
					channel.basicAck(envelope.getDeliveryTag(), false);
				} catch (Exception e) {
					// retrieve death header
					Map<String, Object> headers = properties.getHeaders();
					RabbitMqHeader rabbitMqHeader = new RabbitMqHeader(headers);

					if (rabbitMqHeader.getFailedRetryCount() > FanoutGuideline2OneSubscriber.MAX_RETRY) {
						// publish to dead and ack
						System.err.println("[DEAD] Error at " + new Date() + " on retry "
								+ rabbitMqHeader.getFailedRetryCount() + " for message " + message);

						channel.basicPublish(FanoutGuideline2OneSubscriber.DEAD_EXCHANGE_NAME,
								FanoutGuideline2OneSubscriber.DEAD_EXCHANGE_ROUTING_KEY, null,
								message.getBytes(StandardCharsets.UTF_8.name()));
						channel.basicAck(envelope.getDeliveryTag(), false);
					} else {
						System.err.println("[REQUEUE] Error at " + new Date() + " on retry "
								+ rabbitMqHeader.getFailedRetryCount() + " for message " + message);

						channel.basicReject(envelope.getDeliveryTag(), false);
					}
				}
			}

			private void doWork(String message)
					throws InterruptedException, JsonParseException, JsonMappingException, IOException {
				// convert back to person
				Person person = objectMapper.readValue(message, Person.class);

				System.out.println(new Date() + " - " + this.getClass().getName() + " " + "[runnable "
						+ FanoutGuideline2OneRunnable.this.runnableName + "] Person is : " + person);
			}
		};
		boolean autoAck = false;

		try {
			channel.basicConsume(queueName, autoAck, consumer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}