package com.timpamungkas.rabbitmq;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.timpamungkas.rabbitmq.entity.mybatis.mapper.HelloCustomMapper;
import com.timpamungkas.rabbitmq.entity.mybatis.mapper.HelloMapper;
import com.timpamungkas.rabbitmq.publisher.AmqpHelloPublisher;
import com.timpamungkas.rabbitmq.publisher.AmqpPersonPublisher;

@SpringBootApplication
@MapperScan(basePackages = "com.timpamungkas.rabbitmq.entity.mybatis.mapper")
public class RabbitMQAmqpApplication implements CommandLineRunner {

	@Autowired
	private AmqpHelloPublisher amqpHelloPublisher;
	@Autowired
	private AmqpPersonPublisher amqpPersonPublisher;
	@Autowired
	private HelloMapper helloMapper;
	@Autowired
	private HelloCustomMapper helloCustomMapper;

	public static void main(String[] args) {
		SpringApplication.run(RabbitMQAmqpApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Count() " + helloMapper.countByExample(null));
		System.out.println("Length() " + helloCustomMapper.findByLengthRawMessage(100));

		amqpHelloPublisher.send();
		amqpPersonPublisher.send();
	}

}
