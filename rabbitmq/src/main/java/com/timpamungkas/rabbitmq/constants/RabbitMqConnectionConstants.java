package com.timpamungkas.rabbitmq.constants;

public interface RabbitMqConnectionConstants {

	String HOST = "172.16.16.109";
	int PORT = 5672;
	String USERNAME = "dev";
	String PASSWORD = "password";
	int HANDSHAKE_TIMEOUT = 60000;
	
}
