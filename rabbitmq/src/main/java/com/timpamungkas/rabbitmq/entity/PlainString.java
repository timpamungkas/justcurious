package com.timpamungkas.rabbitmq.entity;

/**
 * @author timpamungkas
 */
public class PlainString {

	private String string;

	public PlainString() {
		super();
	}

	public PlainString(String string) {
		super();
		this.string = string;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlainString other = (PlainString) obj;
		if (string == null) {
			if (other.string != null)
				return false;
		} else if (!string.equals(other.string))
			return false;
		return true;
	}

	public String getString() {
		return string;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((string == null) ? 0 : string.hashCode());
		return result;
	}

	public void setString(String string) {
		this.string = string;
	}

	@Override
	public String toString() {
		return "PlainString [string=" + string + "]";
	}

}
