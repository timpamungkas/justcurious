package com.timpamungkas.rabbitmq.entity.mybatis.mapper;

import java.util.List;

import com.timpamungkas.rabbitmq.entity.mybatis.model.Hello;

public interface HelloCustomMapper {

	/**
	 * Find data with length(raw_message) < parameter
	 * 
	 * @param lengthRawMessage upper treshold, exclusive
	 * @return list of data with length(raw_message) less than 
	 */
	List<Hello> findByLengthRawMessage(int lengthRawMessage);

}