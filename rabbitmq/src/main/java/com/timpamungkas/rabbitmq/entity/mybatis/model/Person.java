package com.timpamungkas.rabbitmq.entity.mybatis.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Person {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column ramq_person.person_id
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	@JsonIgnore
	private Integer personId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column ramq_person.first_name
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	@JsonProperty("first_name")
	private String firstName;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column ramq_person.last_name
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	@JsonProperty("last_name")
	private String lastName;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column ramq_person.birth_date
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	@JsonProperty("birth_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'hh:mm:ss")
	private Date birthDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column ramq_person.creation_date
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	@JsonIgnore
	private Date creationDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column ramq_person.created_by
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	@JsonIgnore
	private Integer createdBy;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column ramq_person.last_updated_date
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	@JsonIgnore
	private Date lastUpdatedDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column ramq_person.last_updated_by
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	@JsonIgnore
	private Integer lastUpdatedBy;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column ramq_person.person_id
	 * @return  the value of ramq_person.person_id
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	public Integer getPersonId() {
		return personId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column ramq_person.person_id
	 * @param personId  the value for ramq_person.person_id
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column ramq_person.first_name
	 * @return  the value of ramq_person.first_name
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column ramq_person.first_name
	 * @param firstName  the value for ramq_person.first_name
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column ramq_person.last_name
	 * @return  the value of ramq_person.last_name
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column ramq_person.last_name
	 * @param lastName  the value for ramq_person.last_name
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column ramq_person.birth_date
	 * @return  the value of ramq_person.birth_date
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	public Date getBirthDate() {
		return birthDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column ramq_person.birth_date
	 * @param birthDate  the value for ramq_person.birth_date
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column ramq_person.creation_date
	 * @return  the value of ramq_person.creation_date
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column ramq_person.creation_date
	 * @param creationDate  the value for ramq_person.creation_date
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column ramq_person.created_by
	 * @return  the value of ramq_person.created_by
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	public Integer getCreatedBy() {
		return createdBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column ramq_person.created_by
	 * @param createdBy  the value for ramq_person.created_by
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column ramq_person.last_updated_date
	 * @return  the value of ramq_person.last_updated_date
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column ramq_person.last_updated_date
	 * @param lastUpdatedDate  the value for ramq_person.last_updated_date
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column ramq_person.last_updated_by
	 * @return  the value of ramq_person.last_updated_by
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column ramq_person.last_updated_by
	 * @param lastUpdatedBy  the value for ramq_person.last_updated_by
	 * @mbg.generated  Thu Jun 21 16:14:17 ICT 2018
	 */
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
}