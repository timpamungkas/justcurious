package com.timpamungkas.rabbitmq.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class RabbitMqHeader {

	private List<RabbitMqHeaderXDeath> xDeaths = new ArrayList<>(2);
	private String xFirstDeathExchange = EMPTY_STRING;
	private String xFirstDeathQueue = EMPTY_STRING;
	private String xFirstDeathReason = EMPTY_STRING;
	private static final String KEYWORD_QUEUE_WAIT = "wait";
	private static final String EMPTY_STRING = "";

	@SuppressWarnings("unchecked")
	public RabbitMqHeader(Map<String, Object> headers) {
		if (headers != null) {
			Optional<Object> xFirstDeathExchange = Optional.ofNullable(headers.get("x-first-death-exchange"));
			Optional<Object> xFirstDeathQueue = Optional.ofNullable(headers.get("x-first-death-queue"));
			Optional<Object> xFirstDeathReason = Optional.ofNullable(headers.get("x-first-death-reason"));

			xFirstDeathExchange.ifPresent(s -> this.setxFirstDeathExchange(s.toString()));
			xFirstDeathQueue.ifPresent(s -> this.setxFirstDeathQueue(s.toString()));
			xFirstDeathReason.ifPresent(s -> this.setxFirstDeathReason(s.toString()));

			List<Map<String, Object>> xDeathHeaders = (List<Map<String, Object>>) headers.get("x-death");

			if (xDeathHeaders != null) {
				for (Map<String, Object> x : xDeathHeaders) {
					RabbitMqHeaderXDeath hdrDeath = new RabbitMqHeaderXDeath();
					Optional<Object> reason = Optional.ofNullable(x.get("reason"));
					Optional<Object> count = Optional.ofNullable(x.get("count"));
					Optional<Object> exchange = Optional.ofNullable(x.get("exchange"));
					Optional<Object> queue = Optional.ofNullable(x.get("queue"));
					Optional<Object> routingKeys = Optional.ofNullable(x.get("routing-keys"));
					Optional<Object> time = Optional.ofNullable(x.get("time"));

					reason.ifPresent(s -> hdrDeath.setReason(s.toString()));
					count.ifPresent(s -> hdrDeath.setCount(Integer.parseInt(s.toString())));
					exchange.ifPresent(s -> hdrDeath.setExchange(s.toString()));
					queue.ifPresent(s -> hdrDeath.setQueue(s.toString()));
					routingKeys.ifPresent(r -> {
						List<String> listR = (List<String>) r;
						hdrDeath.setRoutingKeys(listR);
					});
					time.ifPresent(d -> hdrDeath.setTime((Date) d));

					xDeaths.add(hdrDeath);
				}
			}
		}
	}

	public int getFailedRetryCount() {
		// get from queue "wait"
		for (RabbitMqHeaderXDeath xDeath : xDeaths) {
			if (xDeath.getExchange().toLowerCase().contains(KEYWORD_QUEUE_WAIT)
					&& xDeath.getQueue().toLowerCase().contains(KEYWORD_QUEUE_WAIT)) {
				return xDeath.getCount();
			}
		}

		return 0;
	}

	public List<RabbitMqHeaderXDeath> getxDeaths() {
		return xDeaths;
	}

	public void setxDeaths(List<RabbitMqHeaderXDeath> xDeaths) {
		this.xDeaths = xDeaths;
	}

	public String getxFirstDeathExchange() {
		return xFirstDeathExchange;
	}

	public void setxFirstDeathExchange(String xFirstDeathExchange) {
		this.xFirstDeathExchange = xFirstDeathExchange;
	}

	public String getxFirstDeathQueue() {
		return xFirstDeathQueue;
	}

	public void setxFirstDeathQueue(String xFirstDeathQueue) {
		this.xFirstDeathQueue = xFirstDeathQueue;
	}

	public String getxFirstDeathReason() {
		return xFirstDeathReason;
	}

	public void setxFirstDeathReason(String xFirstDeathReason) {
		this.xFirstDeathReason = xFirstDeathReason;
	}

}
