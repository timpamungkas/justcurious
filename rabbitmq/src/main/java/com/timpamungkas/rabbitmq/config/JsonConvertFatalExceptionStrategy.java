package com.timpamungkas.rabbitmq.config;

import java.util.Date;
import java.util.Map;

import org.springframework.amqp.rabbit.listener.ConditionalRejectingErrorHandler;
import org.springframework.amqp.rabbit.listener.exception.ListenerExecutionFailedException;

import com.timpamungkas.rabbitmq.util.RabbitMqHeader;

public class JsonConvertFatalExceptionStrategy extends ConditionalRejectingErrorHandler.DefaultExceptionStrategy {

	@Override
	public boolean isFatal(Throwable t) {
		if (t instanceof ListenerExecutionFailedException) {
			ListenerExecutionFailedException lefe = (ListenerExecutionFailedException) t;

			// retrieve death header
			String message = new String(lefe.getFailedMessage().getBody());
			Map<String, Object> headers = lefe.getFailedMessage().getMessageProperties().getHeaders();
			RabbitMqHeader rabbitMqHeader = new RabbitMqHeader(headers);

			if (rabbitMqHeader.getFailedRetryCount() > AmqpConfiguration.MAX_RETRY) {
				// publish to dead and ack
				System.err.println("[DEAD] Error at " + new Date() + " on retry " + rabbitMqHeader.getFailedRetryCount()
						+ " for message " + message);

				// channel.basicPublish(FanoutGuideline2OneSubscriber.DEAD_EXCHANGE_NAME,
				// FanoutGuideline2OneSubscriber.DEAD_EXCHANGE_ROUTING_KEY, null,
				// message.getBytes(StandardCharsets.UTF_8.name()));
				// channel.basicAck(envelope.getDeliveryTag(), false);
			} else {
				System.err.println("[REQUEUE] Error at " + new Date() + " on retry "
						+ rabbitMqHeader.getFailedRetryCount() + " for message " + message);

				// channel.basicReject(envelope.getDeliveryTag(), false);
			}
		}

		return super.isFatal(t);
	}

}
